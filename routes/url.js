const express = require("express");
const {
  genrateNewShortUrlHandler,
  getAnalyticsHandler,
} = require("../controllers/url.js");
const router = express.Router();

router.route("/").post(genrateNewShortUrlHandler);
router.route("/analytics/:shortUri").get(getAnalyticsHandler);

module.exports = router;
