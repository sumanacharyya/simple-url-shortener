const express = require("express");
const { getAnalysisShortIdHandler } = require("../controllers/shortId");

const router = express.Router();

router.route("/:shortId").get(getAnalysisShortIdHandler);

module.exports = router;
