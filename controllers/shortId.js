const { URL } = require("../models/url");

const getAnalysisShortIdHandler = async (req, res) => {
  const requestedId = req.params.shortId;
  console.log(requestedId);
  const entryData = await URL.findOneAndUpdate(
    { shortId: requestedId },
    {
      $push: {
        visitHistory: {
          timeStamp: Date.now(),
        },
      },
    }
  );
  res.redirect(`http://${entryData.redirectUrl}`);
};

module.exports = { getAnalysisShortIdHandler };
