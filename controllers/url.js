const xid = require("xid");
const  URL  = require("../models/url");

const genrateNewShortUrlHandler = async (req, res) => {
  const url = req.body?.url;
  console.log(url);
  if (!url) {
    return res.status(401).json({
      error: "A URL is required",
    });
  }
  const shortId = xid.generateId();
  await URL.create({
    shortId,
    redirectUrl: url,
    visitHistory: [],
    createdBy: req.user.id,
  });
  return res.render("home", { id: shortId });
};

const getAnalyticsHandler = async (req, res) => {
  const shortUri = req.params?.shortUri;
  console.log(shortUri);
  if (!shortUri) {
    return res.status(401).json({
      error: "URI is required",
    });
  }
  const clickRes = await URL.findOne({ shortId: shortUri });
  return res.status(200).json({
    totalClocks: clickRes.visitHistory.length,
    analytics: clickRes.visitHistory,
  });
};

module.exports = { genrateNewShortUrlHandler, getAnalyticsHandler };
