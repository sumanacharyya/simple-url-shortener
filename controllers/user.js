const User = require("../models/user");
const { setUser, getUser } = require("../service/auth");

async function handleUserSignup(req, res) {
  const { name, email, password } = req.body;
  const user = await User.create({
    name,
    email,
    password,
  });
  return res.redirect("/");
}

async function handleUserLogin(req, res) {
  const { email, password } = req.body;
  const user = await User.findOne({ email, password });
  if (!user) {
    return res.render("login", {
      message: "Invalid User Name or password!",
    });
  }

  const token = setUser(user);
  res.cookie("token", token);
  // res.cookie("token", token, {
  //   domain: ".localhost:8080",
  // });

  // return res.json({ token });
  return res.redirect("/");
}

module.exports = { handleUserSignup, handleUserLogin };
