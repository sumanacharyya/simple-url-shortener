const { getUser } = require("../service/auth");

const checkForAuthorization = (req, res, next) => {
  // const authorizationHeaderValue = req.headers["authorization"];
  const tokenCookie = req.cookies?.token;

  req.user = null;
  // if (!authorizationHeaderValue || !authorizationHeader.startsWith("Bearer")) {
  //   return next();
  // }
  if (!tokenCookie) {
    return next();
  }
  // const token = authorizationHeaderValue.split("Bearer ")[1];
  const token = tokenCookie;

  const user = getUser(token);
  req.user = user;

  return next();
};

// async function restrictToLoggedInUserOnly(req, res, next) {
//   // const userId = req.cookies?.uid;
//   const userId = req.headers["authorization"];

//   if (!userId) {
//     return res.redirect("/login");
//   }

//   const token = userId.split("Bearer ")[1];
//   const user = getUser(token);

//   if (!user) {
//     return res.redirect("/login");
//   }

//   req.user = user;
//   next();
// }

// async function checkAuth(req, res, next) {
//   // const userId = req.cookies?.uid;
//   // const user = getUser(userId);
//   const userId = req.headers["authorization"];
//   let token;
//   let user;
//   if (userId) {
//     token = userId.split("Bearer ")[1];
//     user = getUser(token);
//   }

//   req.user = user;
//   next();
// }

function restrictTo(roles) {
  // Roles as an array
  return function (req, res, next) {
    console.log(req.user);
    if (!req.user) {
      return res.redirect("/login");
    }
    if (!roles.includes(req.user.role)) {
      return res.end("Unauthorized");
    }
    return next();
  };
}

module.exports = {
  checkForAuthorization,
  restrictTo,
};
