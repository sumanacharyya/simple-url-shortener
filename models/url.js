const { Schema, model } = require("mongoose");

const urlSchema = new Schema(
  {
    shortId: {
      type: "string",
      required: true,
      unique: true,
    },
    redirectUrl: {
      type: "string",
      required: true,
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },

    visitHistory: [
      {
        timeStamp: {
          type: Number,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

const URL = model("Url", urlSchema);
module.exports = URL;

