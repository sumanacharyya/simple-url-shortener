require("dotenv").config();
const express = require("express");
const urlRoute = require("./routes/url");
const shortIdRoute = require("./routes/shortId");
const staticRoute = require("./routes/staticRouter");
const cookieParser = require("cookie-parser");
const userRoute = require("./routes/user");
const path = require("path");
const { connectToMongoDB } = require("./config/connectDB");
const URL = require("./models/url");
const { checkForAuthorization, restrictTo } = require("./middlewares/auth");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(checkForAuthorization);
const PORT = process.env.PORT;

connectToMongoDB(`${process.env.MONGODB_URI}`)
  .then(() => console.log("MongoDB Connected...!"))
  .catch((err) => console.log(err));

app.set("view engine", "ejs");

app.set("views", path.resolve("./views"));

app.get("/test/", async (req, res) => {
  const allUrls = await URL.find({});
  return res.render("home", {
    urls: allUrls,
  });
});

app.use("/url", restrictTo(["NORMAL", "ADMIN"]), urlRoute);

app.use("/url", shortIdRoute);

app.use("/user", userRoute);

app.use("/", staticRoute);

app.listen(PORT, () => {
  console.log(`App listening at port http://localhost:${PORT}`);
});
